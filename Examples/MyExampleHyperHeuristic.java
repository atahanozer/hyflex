package Examples;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import AbstractClasses.HyperHeuristic;
import AbstractClasses.ProblemDomain;

public class MyExampleHyperHeuristic extends HyperHeuristic {

	/**
	 * creates a new ExampleHyperHeuristic object with a random seed
	 */
	public MyExampleHyperHeuristic(long seed) {
		super(seed);
	}

	/**
	 * This method defines the strategy of the hyper-heuristic
	 * @param problem the problem domain to be solved
	 */
	public void solve(ProblemDomain problem) {
		//obtain arrays of the indices of the low level heuristics heuristics which correspond to the different types.
		//the arrays will be set to 'null' if there are no low level heuristics of that type
		int[] local_search_heuristics = problem.getHeuristicsOfType(ProblemDomain.HeuristicType.LOCAL_SEARCH);
		int[] mutation_heuristics = problem.getHeuristicsOfType(ProblemDomain.HeuristicType.MUTATION);
		int[] crossover_heuristics = problem.getHeuristicsOfType(ProblemDomain.HeuristicType.CROSSOVER);
		int[] ruin_recreate_heuristics = problem.getHeuristicsOfType(ProblemDomain.HeuristicType.RUIN_RECREATE);
		int mutation_heuristic_index=-1,crossover_heuristic_index = -1,ruin_recreate_heuristic_index=-1,globalWorseningCounter = 0,diversityWorseningCounter=0;
		ArrayList<int[]> diversification_heuristics = new ArrayList<int[]>();
		ArrayList<Integer> diversificationHeuristics = new ArrayList<Integer>();
		ArrayList<Integer> bannedLocalHeuristics = new ArrayList<Integer>();
		ArrayList<Integer> bannedDiversifyHeuristics = new ArrayList<Integer>();
		ArrayList<Integer> bannedSolutions = new ArrayList<Integer>();
		ArrayList<Double> laList = new ArrayList<Double>();
		for (int i = 0; i < 10; i++) {
			laList.add(Double.POSITIVE_INFINITY);
		}
		System.out.println();
		if(mutation_heuristics != null){
			mutation_heuristic_index = diversification_heuristics.size();
			//diversification_heuristics.add(mutation_heuristics);
			for (int i = 0; i < mutation_heuristics.length; i++) {
				diversificationHeuristics.add(mutation_heuristics[i]);
			}
			System.out.println("Mutation #: "+mutation_heuristics.length);
		}if(crossover_heuristics != null){
			crossover_heuristic_index = diversification_heuristics.size();
			//diversification_heuristics.add(crossover_heuristics);
			System.out.println("Crossover #: "+crossover_heuristics.length);
			for (int i = 0; i < crossover_heuristics.length; i++) {
				diversificationHeuristics.add(crossover_heuristics[i]);
			}
			for (int x = 0; x < crossover_heuristics.length; x++) {
				System.out.print(" c: "+crossover_heuristics[x]);
			}
		
		}if(ruin_recreate_heuristics != null){
			ruin_recreate_heuristic_index = diversification_heuristics.size();
			//diversification_heuristics.add(ruin_recreate_heuristics);
			for (int i = 0; i < ruin_recreate_heuristics.length; i++) {
				diversificationHeuristics.add(ruin_recreate_heuristics[i]);
			}
			System.out.println("Ruin_Recreate #: "+ruin_recreate_heuristics.length);
		}if(local_search_heuristics != null){
			System.out.println("Local search #: "+local_search_heuristics.length);			
			for (int x = 0; x < local_search_heuristics.length; x++) {
				System.out.print(" l: "+local_search_heuristics[x]);
			}
		}	
		System.out.println("\nTime Limit: "+this.getTimeLimit());
		long firstIntervalEnd = this.getTimeLimit()/3;
		long secondIntervalEnd = firstIntervalEnd*2;
		int totalPasses=0,sameBestValueCount=0;
		double lastBestValue=0;
		boolean purgeFinishedFirst = false,purgeFinishedSecond = false;
		
		for (int i = 0; i < diversificationHeuristics.size(); i++) {
			int[] temp;		
			if(mutation_heuristics != null && isCrossover(crossover_heuristics, diversificationHeuristics.get(i))){
				temp = new int[]{diversificationHeuristics.get(i),mutation_heuristics[rng.nextInt(mutation_heuristics.length)]};
			}else{
				temp = new int[]{diversificationHeuristics.get(i)};
			}
			
			diversification_heuristics.add(temp);
		}
		
		//this code is from examplehyperheuristic2, and is explained fully in that example
		int solutionmemorysize = 2;
		problem.setMemorySize(solutionmemorysize+1);
		double[] current_obj_function_values = new double[solutionmemorysize];
		int[] heuristicPoints = new int[problem.getNumberOfHeuristics()];
		int[] diversfyingHeuristicPoints = new int[diversification_heuristics.size()];
		int[] localSearchHeuristicPoints = new int[local_search_heuristics.length];
		int best_solution_index = 0,second_best_solution_index=1;
		double best_solution_value = Double.POSITIVE_INFINITY;
		for (int x = 0; x < solutionmemorysize; x++) {
			problem.initialiseSolution(x);
			current_obj_function_values[x] = problem.getFunctionValue(x);
			laList.add(current_obj_function_values[x]);
			if (current_obj_function_values[x] < best_solution_value) {
				best_solution_value = current_obj_function_values[x];
				best_solution_index = x;
			}
		}
		int heuristic_to_apply=0;
		
		
	/*	problem.setDepthOfSearch(0.5);
		for (int i = 0; i < current_obj_function_values.length; i++) {
			int worseningCounter = 0;
			while(!hasTimeExpired() && worseningCounter < 3){
				//secondly, this hyper-heuristic applies a randomly chosen local search heuristic to the new solution
				heuristic_to_apply = 0;
				//we must check that there are some local search heuristics in this problem domain
				if (local_search_heuristics != null) {
					int cnt=0;
					do{
						heuristic_to_apply = local_search_heuristics[rng.nextInt(local_search_heuristics.length)];
						cnt++;
					}while(bannedLocalHeuristics.contains(new Integer(heuristic_to_apply)) && cnt <10);
					
				} else {//we apply a randomly selected heuristic if there are no local searchers
					heuristic_to_apply = rng.nextInt(problem.getNumberOfHeuristics());
				}
				
				current_obj_function_values[i] = problem.applyHeuristic(heuristic_to_apply, best_solution_index, solutionmemorysize-1);
	
				//if the solution is better or equal, copy to best solution index
				if (current_obj_function_values[i] < best_solution_value || ( laList.size() == 0 || current_obj_function_values[i] < laList.get(laList.size()-1))) {
					problem.copySolution(solutionmemorysize-1, i);
					best_solution_value	= current_obj_function_values[i];
					best_solution_index = i;
					heuristicPoints[heuristic_to_apply]++;
					if(current_obj_function_values[i] >= best_solution_value){
						laList.add(0, current_obj_function_values[i]);
						if(laList.size() > 0)
							laList.remove(laList.size()-1);
					}
					//globalWorseningCounter--;
				}else{
					worseningCounter++;
					globalWorseningCounter++;
					if (current_obj_function_values[i] > best_solution_value) {
						heuristicPoints[heuristic_to_apply]--;
					}
					laList.add(0, laList.get(0));
					if(laList.size() > 0)
						laList.remove(laList.size()-1);
					//System.out.println("worseningCounter "+worseningCounter);
				}
			}
			//one iteration has been completed, so we return to the start of the main loop and check if the time has expired 
		 }*/
		problem.setDepthOfSearch(0.2);
		//the main loop of any hyper-heuristic, which checks if the time limit has been reached
		while (!hasTimeExpired()) {
			/*for (int i = 0; i < diversification_heuristics.size(); i++) {
				System.out.println("i: "+ i + " value: "+diversfyingHeuristicPoints[i]);
			}*/
		/*	for (int i = 0; i < current_obj_function_values.length; i++) {
				System.out.println("i: "+ i + " value: "+current_obj_function_values[i]);
			}
			System.out.println();
			for (int i = 0; i < heuristicPoints.length; i++) {
				System.out.print(" ____ "+ i + " hp: "+heuristicPoints[i]);
			}
			*/
		/*	System.out.println();
			for (int i = 0; i < laList.size(); i++) {
				System.out.print(" ______ "+ i + " la : "+laList.get(i));
			}
			System.out.println();
			*/
			if((!purgeFinishedFirst && firstIntervalEnd <= this.getElapsedTime()) || (!purgeFinishedSecond && secondIntervalEnd <= this.getElapsedTime()) ){
				System.out.println("Elapsed Time: "+this.getElapsedTime());
				if(!purgeFinishedFirst)
					purgeFinishedFirst = true;
				else
					purgeFinishedSecond = true;
					
				//copy diversing heuristics
				if(diversfyingHeuristicPoints.length >=2){
					int temp = getWorstDiversifyHeuristic(diversfyingHeuristicPoints);
					bannedDiversifyHeuristics.add(temp);
			//		System.out.println("Banned Diversifier: "+temp);
				}
				
				if(local_search_heuristics.length>=3){
					//copy hill climbing heuristics
					int min=1000000,minIndex=-1;
					for (int i = 0; i < local_search_heuristics.length; i++) {
						if(heuristicPoints[local_search_heuristics[i]] < min){
							min = heuristicPoints[local_search_heuristics[i]];
							minIndex = i;
						}				
					}
					bannedLocalHeuristics.add(local_search_heuristics[minIndex]);
				//	System.out.println("Banned Local Search: "+local_search_heuristics[minIndex]);
				}	
				if(bannedSolutions.size() < current_obj_function_values.length-2){
				int minIndex=-1;
				double min=Double.POSITIVE_INFINITY;
				if(!purgeFinishedSecond){
					for (int i = 0; i < current_obj_function_values.length; i++) {
						if(current_obj_function_values[i] < min && !bannedSolutions.contains(i)){
							min = current_obj_function_values[i];
							minIndex = i;
						}				
					}
					bannedSolutions.add(minIndex);
				}else{
					double max=0;
					for (int i = 0; i < current_obj_function_values.length; i++) {
						if(current_obj_function_values[i] > max && !bannedSolutions.contains(i)){
							max = current_obj_function_values[i];
							minIndex = i;
						}				
					}
					for (int i = 0; i < current_obj_function_values.length; i++) {
						if(i != minIndex && !bannedSolutions.contains(i)){
							bannedSolutions.add(i);
						}				
					}
					
				}
				}
			}
			for (int i = 0; i < bannedSolutions.size(); i++) {
				System.out.println("Banned: "+ i + " Banned solutions : "+bannedSolutions.get(i)+" --- "+current_obj_function_values[bannedSolutions.get(i)]);
			}
					
		//	System.out.println("globalWorseningCounter: "+globalWorseningCounter+"----------------------------------------");
			
			if(globalWorseningCounter >= 8){
				//globalWorseningCounter=0;
				if(problem.getIntensityOfMutation() < 0.9){
					problem.setIntensityOfMutation(problem.getIntensityOfMutation()+0.1);
				//	System.out.println("Mutation intesnisty #: "+problem.getIntensityOfMutation());
				}
			}else if(globalWorseningCounter < -8){
				//globalWorseningCounter=0;
				if(problem.getIntensityOfMutation() > 0.3){
					problem.setIntensityOfMutation(problem.getIntensityOfMutation()-0.1);
				//	System.out.println("Mutation intesnisty #: "+problem.getIntensityOfMutation());
				}
			}
			
		if(diversification_heuristics.size() > 0){
			for (int i = 0; i < current_obj_function_values.length; i++) {
				if(bannedSolutions.contains(new Integer(i)))
					continue;
				//Random diverifier sec
				int [] currentDiversifierArray;
				int temp;
				int cnt=0;
				do{
					temp = rng.nextInt(diversification_heuristics.size());
					currentDiversifierArray = diversification_heuristics.get(temp);
					cnt++;
				}while(bannedDiversifyHeuristics.contains(new Integer(temp)) && cnt<10);
							
				for (int j = 0; j < currentDiversifierArray.length; j++) {
					
					heuristic_to_apply = currentDiversifierArray[j];
				
			if(isCrossover(crossover_heuristics, heuristic_to_apply)){
				//we now perform a crossover heuristic on two randomly selected solutions
				int solution_index_1 = i;
				int solution_index_2 = best_solution_index;
				if (solution_index_1 == solution_index_2 && solutionmemorysize > 1) {
					solution_index_2 = solutionmemorysize-1;
				}
				//if the solutions are the same, choose a different one
				while (true && solutionmemorysize > 3) {
					if (solution_index_1 == solution_index_2 && solutionmemorysize > 1) {
						solution_index_2 = rng.nextInt(solutionmemorysize-1);
					} else {break;}}

				//we select a random crossover heuristic to use
				heuristic_to_apply = crossover_heuristics[rng.nextInt(crossover_heuristics.length)];

				//the method to apply crossover heuristics involves specifying two indices of solutions in the memory, and an index into which to put the result of the crossover.
				//in this example we give the randomly selected indices as input, and we overwrite the first parent with the resulting solution
					current_obj_function_values[solution_index_1] = problem.applyHeuristic(heuristic_to_apply, solution_index_1, solution_index_2, solution_index_1);

				//if the result of the crossover is better than the best solution, then update the best solution record
				if (current_obj_function_values[solution_index_1] < best_solution_value) {
					heuristicPoints[heuristic_to_apply]++;
				}else{
					heuristicPoints[heuristic_to_apply]--;
				}
			}else{
				//apply the randomly chosen heuristic to the current best solution in the memory
				
					//we accept every move, so the new solution can be immediately written to the same index in memory// not any more
					if(currentDiversifierArray.length > 1 && j == currentDiversifierArray.length-1)//if we are dealing with first of the pair, apply to best solution, else apply to current i.
						current_obj_function_values[i] = problem.applyHeuristic(heuristic_to_apply, solutionmemorysize-1, solutionmemorysize-1);
					else
						current_obj_function_values[i] = problem.applyHeuristic(heuristic_to_apply, best_solution_index,solutionmemorysize-1);

			}
		 }
				if(current_obj_function_values[i] < best_solution_value || (current_obj_function_values[i] < laList.get(laList.size()-1))){
					problem.copySolution(solutionmemorysize-1, i);
					diversfyingHeuristicPoints[i]++;
					if(current_obj_function_values[i] < best_solution_value){
						best_solution_value = current_obj_function_values[i];
						best_solution_index = i;
						diversityWorseningCounter--;
					}
					//if(current_obj_function_values[i] >= best_solution_value){
						laList.add(0, current_obj_function_values[i]);
						laList.remove(laList.size()-1);
					//}
					//globalWorseningCounter--;
				}else if(current_obj_function_values[i] >= best_solution_value){
					diversfyingHeuristicPoints[i]--;
					diversityWorseningCounter++;
					laList.add(0, laList.get(0));
					laList.remove(laList.size()-1);
					globalWorseningCounter++;
					if(globalWorseningCounter>10 && Double.compare(laList.get(laList.size()-1),laList.get(0)) == 0){
						for (int n = 0; n < laList.size(); n++) {
							laList.set(n, laList.get(n)*(rng.nextInt(3)+2));
						}
						if(problem.getIntensityOfMutation() <0.9)
							problem.setIntensityOfMutation(problem.getIntensityOfMutation()+0.1);
					}
				}
		}
		}else{
			System.out.println("diversification_heuristics.size() "+diversification_heuristics.size());
		}
		
		//System.out.println("Hillclimbing begins... "+this.getElapsedTime()+" "+problem.getBestSolutionValue()+" Same passes: "+sameBestValueCount+" Total Pass: "+totalPasses);
		totalPasses++;
		
		if(Double.compare(lastBestValue,problem.getBestSolutionValue()) == 0){
			sameBestValueCount++;
		}else{
			sameBestValueCount=0;
			lastBestValue = problem.getBestSolutionValue();
		}
		if(totalPasses > 30){
			if(this.getElapsedTime() > this.getTimeLimit()/2 && sameBestValueCount > totalPasses/2){
				sameBestValueCount=0;
				totalPasses=0;
				for (int i = 0; i < laList.size(); i++) {
					laList.set(i, Double.POSITIVE_INFINITY);
					if(i > 9){
						laList.remove(i);
					}
				}
				
				problem.setIntensityOfMutation(0.8);
				problem.setDepthOfSearch(0.2);
				System.out.print("Restarted... ");
			}
		}
		
	//Hillclimbing	
		for (int j = 0; j < local_search_heuristics.length-1; j++) {
			//if(bannedSolutions.contains(new Integer(i)))
				//continue;
			if(bannedLocalHeuristics.contains(new Integer(local_search_heuristics[j])))
					continue;
			int i=0;
			int worseningCounter = 0;
			while(!hasTimeExpired() && worseningCounter < 3){
				//secondly, this hyper-heuristic applies a randomly chosen local search heuristic to the new solution
				heuristic_to_apply = 0;
				//we must check that there are some local search heuristics in this problem domain
				if (local_search_heuristics != null) {
					heuristic_to_apply = local_search_heuristics[i];
				/*	int cnt=0;
					do{
						heuristic_to_apply = local_search_heuristics[rng.nextInt(local_search_heuristics.length)];
						cnt++;
					}while(bannedLocalHeuristics.contains(new Integer(heuristic_to_apply)) && cnt <10);
					*/
				} else {//we apply a randomly selected heuristic if there are no local searchers
					heuristic_to_apply = rng.nextInt(problem.getNumberOfHeuristics());
				}
				
				current_obj_function_values[i] = problem.applyHeuristic(heuristic_to_apply, best_solution_index, solutionmemorysize-1);
	
				//if the solution is better or equal, copy to best solution index
				if (current_obj_function_values[i] < best_solution_value || ( laList.size() == 0 || current_obj_function_values[i] < laList.get(laList.size()-1))) {
					problem.copySolution(solutionmemorysize-1, i);
					best_solution_value	= current_obj_function_values[i];
					best_solution_index = i;
					heuristicPoints[heuristic_to_apply]++;
					if(current_obj_function_values[i] >= best_solution_value){
						laList.add(0, current_obj_function_values[i]);
						if(laList.size() > 0)
							laList.remove(laList.size()-1);
					}
					//globalWorseningCounter--;
				}else{
					worseningCounter++;
					globalWorseningCounter++;
					if (current_obj_function_values[i] > best_solution_value) {
						heuristicPoints[heuristic_to_apply]--;
					}
					laList.add(0, laList.get(0));
					if(laList.size() > 0)
						laList.remove(laList.size()-1);
					//System.out.println("worseningCounter "+worseningCounter);
				}
				
				if(globalWorseningCounter>10){
					globalWorseningCounter=0;
					if(laList.size() < 20){
						laList.add(laList.get(0)*5);
					}else{
						problem.setIntensityOfMutation(0.9);
						problem.setDepthOfSearch(0.7);
					}
					if(Double.compare(laList.get(laList.size()-1),laList.get(0)) == 0){
						for (int n = 0; n < laList.size(); n++) {
							laList.set(n, laList.get(n)*(rng.nextInt(3)+2));
						}
					}
					if(problem.getDepthOfSearch() <= 0.7){
						problem.setDepthOfSearch(problem.getDepthOfSearch()+0.1);
						//System.out.println("Depthof search increased #: "+problem.getDepthOfSearch());
					}
				}else if(globalWorseningCounter<-10){
					if(laList.size() > 0)
						laList.remove(laList.size()-1);
					globalWorseningCounter=0;
					if(problem.getDepthOfSearch() > 0.3){
						problem.setDepthOfSearch(problem.getDepthOfSearch()-0.1);
					//	System.out.println("Depthof search decreased #: "+problem.getDepthOfSearch());
					}
				}
			}
			//one iteration has been completed, so we return to the start of the main loop and check if the time has expired 
		 }
		}
		System.out.println();
	}

	/**
	 * this method must be implemented, to provide a different name for each hyper-heuristic
	 * @return a string representing the name of the hyper-heuristic
	 */
	public String toString() {
		return "My Example Heuristic";
	}
	
	/**
	 * this method checks if given heuristic is a Crossover heuristic.
	 * @return a boolean
	 */
	public boolean isCrossover(int[] crossover_heuristics,int heuristic) {
		for (int i = 0; i < crossover_heuristics.length; i++) {
			if(crossover_heuristics[i] == heuristic)
				return true;
		}
		return false;
	}
	
	public int getWorstDiversifyHeuristic(int[] diversfyingHeuristicPoints) {
		int minPoint = 100000,minIndex=-1;
		for (int i = 0; i < diversfyingHeuristicPoints.length; i++) {
			if(diversfyingHeuristicPoints[i] < minPoint){
				minIndex=i;
				minPoint=diversfyingHeuristicPoints[i];
			}	
		}
		return minIndex;
	}

}
